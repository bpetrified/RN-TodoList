import { applyMiddleware, combineReducers, createStore } from 'redux';
//middleware
import createSagaMiddleware from 'redux-saga';
//reducer
import { reducer as formReducer } from 'redux-form';
import todoList from './todo-list/todo-list.reducer';
//saga
import rootSaga from './saga';

const sagaMiddleware = createSagaMiddleware();

const appReducer = combineReducers({
    form: formReducer,
    todoList
});


const store = createStore(
    appReducer,
    applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(rootSaga);

export default store;



