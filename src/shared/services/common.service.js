import moment from 'moment';

class CommonService {
    constructor(){

    }

    formatDate(date){
        return date ? moment(date).format('DD-MM-YYYY hh:mm') : '';
    }
}

export default new CommonService();