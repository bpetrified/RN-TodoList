import { AsyncStorage } from 'react-native';

const INCREMENT = 'INCREMENT';

class ItemService {
    constructor(){
    }

    async getItems(){
       let items = [];
       const keys = await AsyncStorage.getAllKeys();
       await Promise.all(keys.map(async (key) => {
            if(key !== INCREMENT){
                const item = await AsyncStorage.getItem(key);
                items.push(JSON.parse(item));
            }
       }));
       return items;
    }

    async save(item){
        const defaultItem = {
            completed: false
        };
        if(!item.id){
            const lastId = await AsyncStorage.getItem(INCREMENT);
            item.id = Number(lastId) + 1 + '';
        }
        item = Object.assign(defaultItem, item);
        await AsyncStorage.setItem(item.id, JSON.stringify(item));
        await AsyncStorage.setItem(INCREMENT, item.id);
    }

    async remove(){
        await AsyncStorage.removeItem(item.id);
    }

    async init(){
        const increment = await AsyncStorage.getItem(INCREMENT);
        if(!increment){
            await AsyncStorage.setItem(INCREMENT, '0');
        }
    }
}

export default new ItemService();