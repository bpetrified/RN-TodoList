import React from 'react';
import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    disabled:{
        opacity:0.5
    },
    button:{
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5
    }
})

export default styles;