import React, {Component} from 'react';
import {TouchableOpacity, View} from 'react-native';
import styles from './button.component.style';

export default class Button extends Component {
    render() {
        const {disabled, onPress, accessibilityLabel, testID} = this.props;
        return (
            <View style={disabled ? styles.disabled : ''}>
                <TouchableOpacity accessible={false} disabled={disabled} activeOpacity={1} onPress={() => {
                    if (disabled) {
                        return;
                    }
                    onPress()
                }} style={[styles.button, this.props.style]}
                                  accessibilityLabel={accessibilityLabel + (disabled ? '_disabled' : '')
                                  } testID={testID + (disabled ? '_disabled' : '')}>
                    {this.props.children}
                </TouchableOpacity>
            </View>
        )
    }
}