import React from 'react';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  customText: {
    fontSize: 16,
    backgroundColor: 'rgba(0,0,0,0)'
  }
});

export default styles;