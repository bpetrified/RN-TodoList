import React, {Component} from 'react';
import {Platform, Text} from 'react-native';
import styles from './custom-text.component.style';
import typoStyles from '../../styles/typo';

export default class CustomText extends Component {
  render() {
    return (
      <Text {...this.props} style={[styles.customText, this.props.style]}
        //on ios if we pass "accessibilityLabel" it will break automation
        accessibilityLabel={Platform.OS === 'ios' ? undefined : this.props.accessibilityLabel}
      >{this.props.children}</Text>
    )
  }
}