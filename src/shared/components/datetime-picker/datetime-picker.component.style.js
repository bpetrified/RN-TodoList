import React from 'react';
import {StyleSheet} from 'react-native';
import appConstants from '../../../constant';

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems:'center'
    },
    icon: {
        width:20, height: 20, marginLeft: 10
    }
})

export default styles;