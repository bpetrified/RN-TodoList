import React, { Component } from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
//styles
import styles from './datetime-picker.component.style';
//components
import CustomText from '../custom-text/custom-text.component';
//services
import commonService from '../../services/common.service';

//to be used with redux-form
export default class DatetimePicker extends Component {
    state = {
        isVisible: false
    }
    render() {
        const { input: { onChange, value } } = this.props;
        return (
            <View>
                <View style={styles.container}>
                    <CustomText>{commonService.formatDate(value)}</CustomText>
                    <TouchableOpacity onPress={() => { 
                        this.setState({ isVisible: true });
                     }}>
                        <Image style={styles.icon} source={require('../../../../assets/images/calendar.png')}

                        />
                    </TouchableOpacity>
                </View>
                <DateTimePicker
                    isVisible={this.state.isVisible}
                    onConfirm={(date) => { onChange(date); this.setState({ isVisible: false }); }}
                    onCancel={() => { this.setState({ isVisible: false }); }}
                    mode="datetime"
                /></View>)
    }
}