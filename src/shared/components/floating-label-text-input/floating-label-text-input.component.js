import React, {Component} from 'react';
import {Animated, Platform, TextInput, View} from 'react-native';
import typoStyles from '../../styles/typo';
import styles from './floating-label-text-input.component.style';
import appConstants from '../../../constant';

/**
 *  To be used inside redux form, to used it in another way, you might need to map value and change event yourself
 * **/
export default class FloatingLabelInput extends Component {
    state = {
        isFocused: false,
    };

    handleFocus = () => this.setState({ isFocused: true });
    handleBlur = () => this.setState({ isFocused: false });

    render() {
        //Size config
        const sizes = {
            medium: {
                wrapperStyle:{
                    paddingTop:18
                },
                inputStyle:{
                    lineHeight: 26,
                    fontSize: 20
                },
                labelFontSizeInactive: 16,
                labelFontSizeActive: 14,
                labelTopIOSInactive: 23,
                labelTopAndroidInactive: 23,
                labelTopActive: 0
            },
            small:{
                wrapperStyle:{
                    paddingTop:18
                },
                inputStyle:{
                    lineHeight: 15,
                    fontSize: 12
                },
                labelFontSizeInactive: 20,
                labelFontSizeActive: 14,
                labelTopIOSInactive: 10,
                labelTopAndroidInactive: 10,
                labelTopActive: 0
            }
        }
        let configuredSize = sizes.medium;
        configuredSize = (this.props.size && sizes[this.props.size]) ? sizes[this.props.size] : configuredSize;
        
        const { label, isError, input: { value, onChange },
            meta: { error }, accessibilityLabel, testID, secureTextEntry, keyboardType, autoCapitalize,
            multiline, maxLength,
            inputStyle, wrapperStyle, inputFocusedStyle, labelActiveColor, labelInactiveColor,//custom styles
            labelTopIOSInactive, labelTopAndroidInactive, size, parentHeight
        } = this.props;
        const { isFocused } = this.state;
        const labelStyle = {
            position: 'absolute',
            backgroundColor: 'transparent',
            left: 0,
            top: this._animatedIsFocused.interpolate({
                inputRange: [0, 1],
                outputRange: [Platform.OS === 'ios' ?
                labelTopIOSInactive || configuredSize.labelTopIOSInactive : //ios
                labelTopAndroidInactive || configuredSize.labelTopAndroidInactive, //android
                configuredSize.labelTopActive],
            }),
            fontSize: this._animatedIsFocused.interpolate({
                inputRange: [0, 1],
                outputRange: [configuredSize.labelFontSizeInactive, configuredSize.labelFontSizeActive],
            }),
            color: this._animatedIsFocused.interpolate({
                inputRange: [0, 1],
                outputRange: [isError ? appConstants.color.error : labelInactiveColor || appConstants.color.gray, labelActiveColor || appConstants.color.gray],
            }),
        };
        return (
            <View style={[configuredSize.wrapperStyle, wrapperStyle]} accessibilityLabel={accessibilityLabel} testID={testID}>
                <Animated.Text style={[labelStyle]}>
                    {label}
                </Animated.Text>
                <TextInput
                    style={[styles.textInput, configuredSize.inputStyle,
                    inputStyle, 
                    isFocused ? typoStyles.primaryBlue : '',
                    isFocused ? { borderBottomColor: appConstants.color.primaryBlue } : '',
                    isFocused && inputFocusedStyle ? inputFocusedStyle : '',
                    isError ? typoStyles.error : '',
                    isError ? { borderBottomColor: appConstants.color.error } : '',
                    multiline ? { height: this.state.height } : '',
                    ]
                    //end style
                    }
                    keyboardType={keyboardType}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    onChangeText={onChange}
                    underlineColorAndroid={'transparent'}
                    autoComplete={false}
                    autoCorrect={false}
                    autoCapitalize={autoCapitalize || 'sentences'}
                    value={value}
                    secureTextEntry={secureTextEntry}
                    multiline={multiline}
                    maxLength={maxLength}
                    onContentSizeChange={(e) => this.updateSize(e.nativeEvent.contentSize.height, parentHeight - configuredSize.wrapperStyle.paddingTop)}
                />
            </View>
        );
    }

    componentWillMount() {
        this._animatedIsFocused = new Animated.Value(this.props.input.value === '' ? 0 : 1);
    }

    componentDidUpdate() {
        Animated.timing(this._animatedIsFocused, {
            toValue: (this.state.isFocused | this.props.input.value !== '') ? 1 : 0,
            duration: 200,
        }).start();
    }

    updateSize = (height, parentHeight) => {
        const newHeight = (!parentHeight || (height <= parentHeight)) ? height : parentHeight;
        this.setState({
          height: newHeight
        });
    }
}