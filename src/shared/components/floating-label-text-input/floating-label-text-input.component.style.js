import React from 'react';
import {StyleSheet} from 'react-native';
import appConstants from '../../../constant';

const styles = StyleSheet.create({
    textInput: {
        height: 26,
        lineHeight: 26,
        fontSize: 20,
        paddingBottom: 0,
        paddingTop: 0,
        paddingLeft: 0,
        color: '#414141',
        borderBottomWidth: 1,
        borderBottomColor: appConstants.color.lightGray,
    },

    error: {
        color: appConstants.color.error,
        borderBottomColor: appConstants.color.error
    }
})

export default styles;