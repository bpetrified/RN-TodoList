import React, {Component} from 'react';
import {ActivityIndicator, View} from 'react-native';
import style from './loading-overlay.component.style';

export default class LoadingOverlay extends Component {
  render() {
    return (
      <View style={[style.overlay, this.props.overlayStyle]}>
        <ActivityIndicator {...this.props}/>
      </View>
    )
  }
}