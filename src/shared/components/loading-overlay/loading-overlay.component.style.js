import React from 'react';
import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    overlay: {
        position: 'absolute',
        right: 0,
        top: 0,
        left: 0,
        bottom: 0,
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default styles;