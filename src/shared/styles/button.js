import React from 'react';
import {StyleSheet} from 'react-native';
import appConstant from '../../constant.js';

const styles = StyleSheet.create({
    primaryBlue: {
        backgroundColor: appConstant.color.primaryBlue
    },
    outlineBlue: {
        backgroundColor: 'white',
        borderWidth: 2,
        borderColor: appConstant.color.primaryBlue
    },
    red:{
        backgroundColor: appConstant.color.error,
        borderColor: appConstant.color.error
    }
})

export default styles;