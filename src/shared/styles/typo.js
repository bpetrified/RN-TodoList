import React from 'react';
import {StyleSheet} from 'react-native';
import appConstant from '../../constant';

const styles = StyleSheet.create({
    //size
    superSmall: {
        fontSize: 10
    },
    small: {
        fontSize: 14
    },
    medium: {
        fontSize: 16
    },
    large: {
        fontSize: 20
    },
    title: {
        fontSize: 28
    },
    //color
    primaryBlue: {
        color: appConstant.color.primaryBlue
    },
    error:{
        color: appConstant.color.error
    },
    white: {
        color: 'white'
    },
    gray: {
        color: appConstant.color.gray
    },
    darkGray: {
        color: appConstant.color.darkGray
    },
    //align
    center:{
        textAlign: 'center'
    },
    left:{
        textAlign: 'left'
    }
})

export default styles;