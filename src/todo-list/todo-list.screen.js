import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//action
import todoListActions from './todo-list.action';
//components
import TodoList from './todo-list.component';

export class TodoListScreenComponent extends Component {
    componentDidMount(){
      this.props.actions.getItems();
    }

    render() {
        return (<TodoList {...this.props}/>)
    }
}

export const mapStateToProps = ({ todoList }) => ({
  state: {
    ...todoList
  }
});

export const mapDispatchToProps = dispatch => ({ 
  actions: bindActionCreators(todoListActions, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(TodoListScreenComponent);