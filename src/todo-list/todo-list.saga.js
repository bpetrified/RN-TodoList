import { call, put, takeLatest } from 'redux-saga/effects';
import todoListConstants from './todo-list.constant';
import todoListActions from './todo-list.action';
//services
import itemService from '../shared/services/item.service';

export function* doSave(action) {
    try {
        const item = action.data;
        yield call(itemService.save, item);
        yield call(doGetItems);
        yield put(todoListActions.saveFinish());
        yield put(todoListActions.closeModal());
    } catch (e) {
        console.log('[error][saga][doSave]', e);
    }
}

export function* doGetItems() {
    try {
        const items = yield call(itemService.getItems);
        yield put(todoListActions.getItemsFinish(items));
    } catch (e) {
        console.log('[error][saga][doGetItems]', e);
    }
}

export default todoListSagas = [
    takeLatest(todoListConstants.save, doSave),
    takeLatest(todoListConstants.getItems, doGetItems)
];

