import todoListConstants from './todo-list.constant';

const initialState = {
  modalShown: false,
  selectedItem: null,
  loading: false,
  items: [],
  selectedFilters: [...todoListConstants.filterList]
}

export default function todoList(state = initialState, action) {
  switch (action.type) {
    case todoListConstants.showModal:
      return {
        ...state,
        modalShown: true,
        selectedItem: action.data
      };
    case todoListConstants.closeModal:
      return {
        ...state,
        modalShown: false,
        selectedItem: null
      };
    case todoListConstants.getItems:
    case todoListConstants.save:
    case todoListConstants.remove:
      return {
        ...state,
        //loading: true
      }
    case todoListConstants.getItemsFinish:
      return {
        ...state,
        //loading: false,
        items: action.data
      };
    case todoListConstants.toggleFilter:
      //remove if exist/push if not
      let index = state.selectedFilters.findIndex((_filter) => {
        return _filter.id === action.data.id;
      });
      if (index === -1) {
        state.selectedFilters.push(action.data);
      } else {
        state.selectedFilters.splice(index, 1);
      }
      return {
        ...state,
        selectedFilters: [
          ...state.selectedFilters
        ]
      }
    default:
      return state
  }
}