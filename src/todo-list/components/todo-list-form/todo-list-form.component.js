import React, { Component } from 'react';
import { View, Keyboard } from 'react-native';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
//components
import Button from '../../../shared/components/button/button.component';
import CustomText from '../../../shared/components/custom-text/custom-text.component';
import FloatingLabelInput from '../../../shared/components/floating-label-text-input/floating-label-text-input.component';
import DatetimePicker from '../../../shared/components/datetime-picker/datetime-picker.component';
//styles
import styles from './todo-list-form.component.style';
import typoStyles from '../../../shared/styles/typo';
import buttonStyles from '../../../shared/styles/button';

export const validate = values => {
    const errors = {}
    if (!values.title) {
        errors.title = 'Required';
    }
    return errors
};

class TodoListForm extends Component {
    render() {
        const { handleSubmit, onSave, invalid,
            initialValues = initialValues || {}
        } = this.props;

        return (
            <View>
                {initialValues.id && <CustomText style={typoStyles.large}>Item ID:{initialValues.id}</CustomText>}
                
                <Field name="title" component={FloatingLabelInput}
                    label={'Title'} wrapperStyle={{ marginTop: 20 }}
                    inputStyle={{ borderBottomColor: appConstants.color.gray }}
                    autoCapitalize="none" />

                <Field name="desc" component={FloatingLabelInput}
                    label={'Description'} wrapperStyle={{ marginTop: 20 }}
                    inputStyle={{ borderBottomColor: appConstants.color.gray }}
                    autoCapitalize="none" />

                <Field name="date" component={DatetimePicker}
                    date={initialValues.date} mode="datetime"
                />
                <Button style={[buttonStyles.primaryBlue, styles.button]}
                    disabled={invalid}
                    onPress={handleSubmit((values) => { 
                        Keyboard.dismiss(); 
                        onSave(values);
                })}>
                    <CustomText style={typoStyles.white}>SAVE</CustomText>
                </Button>
            </View>
        )
    }
}


let form = reduxForm({
    form: 'todoListForm',
    validate
})(TodoListForm);

export default connect(
    state => ({
        initialValues: state.todoList.selectedItem, // pull initial values from account reducer
    }), null
)(form);