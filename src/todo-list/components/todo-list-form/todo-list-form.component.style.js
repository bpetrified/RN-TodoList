import React from 'react';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
   button: {
       height: 50,
       marginTop: 20
   }
});

export default styles;