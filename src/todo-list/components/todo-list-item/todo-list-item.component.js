import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native';
//styles
import styles from './todo-list-item.component.style';
import typoStyles from '../../../shared/styles/typo';
//components
import CustomText from '../../../shared/components/custom-text/custom-text.component';
import CheckBox from 'react-native-check-box';
//services
import commonService from '../../../shared/services/common.service';

export default class TodoListItem extends Component {
    render() {
        const { item, onPress, onCheckboxChanged } = this.props;

        return (
            <TouchableOpacity activeOpacity={1} style={styles.container} 
                onPress={()=>{
                    onPress(item);
                }
                }
            >
                <CheckBox
                    style={styles.itemCheck}
                    onClick={() => { 
                        item.completed = !item.completed;
                        onCheckboxChanged(item);
                    }}
                    isChecked={item.completed}
                />
                <View style={styles.itemTextContainer}>
                    <CustomText>{item.title}</CustomText>
                    {item.date && <CustomText style={typoStyles.small}>{commonService.formatDate(item.date)}</CustomText>}
                </View>
            </TouchableOpacity>
        );
    }
}