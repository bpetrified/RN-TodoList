import React from 'react';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        height: 50,
        borderBottomWidth: 1,
        borderBottomColor: 'gray'
    },
    itemTextContainer: {
        flex: 1, padding: 5, justifyContent: 'center'

    },
    itemCheck: {
        width: 50, height: 50, padding: 12
    }
});

export default styles;