import React from 'react';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row'
    },
    buttonContainer:{
        flex:0.5,
        margin: 10
    },
    button: {
        height:40
    }
});

export default styles;