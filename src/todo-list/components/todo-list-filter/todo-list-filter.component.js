import React, { Component } from 'react';
import { View } from 'react-native';
import todoListConstants from '../../todo-list.constant';
//styles
import styles from './todo-list-filter.component.style';
import buttonStyles from '../../../shared/styles/button';
import typoStyles from '../../../shared/styles/typo';
//components
import CustomText from '../../../shared/components/custom-text/custom-text.component';
import Button from '../../../shared/components/button/button.component';

let _this;
export default class TodoListFilter extends Component {
    render() {
        _this = this;
        const { onFilterChanged,
            selectedFilters = selectedFilters || []
        } = this.props;

        return (<View style={styles.container}>
            {todoListConstants.filterList.map((_filter) => {
                return (<View style={styles.buttonContainer} key={_filter.id}>
                    <Button
                        onPress={() => { onFilterChanged(_filter); }}
                        style={[
                            styles.button,
                            _this.isActive(_filter, selectedFilters) ? buttonStyles.primaryBlue : buttonStyles.outlineBlue
                        ]}
                    >
                        <CustomText 
                            style={[
                                typoStyles.small,
                                _this.isActive(_filter, selectedFilters) ? typoStyles.white : typoStyles.primaryBlue
                            ]}
                        >
                            {_filter.label}
                        </CustomText>
                    </Button></View>)
            })}
        </View>);
    }

    isActive(filter, selectedFilters) {
        return selectedFilters.findIndex((_filter) => {
            return _filter.id === filter.id;
        }) !== -1;
    }
}

