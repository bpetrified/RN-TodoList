import React, { Component } from 'react';
import { FlatList } from 'react-native';
//styles
import styles from './todo-list-flatlist.component.style';
//components
import CustomText from '../../../shared/components/custom-text/custom-text.component';
import TodoListItem from '../todo-list-item/todo-list-item.component';

export default class TodoListFlatList extends Component {
    render() {
        const { onItemPress, datas, onCheckboxChanged, selectedFilters } = this.props;

        return (
            <FlatList
                data={this.filterItems(datas, selectedFilters)}
                renderItem={({ item }) => <TodoListItem item={item}
                    onPress={onItemPress} onCheckboxChanged={onCheckboxChanged} />}
                keyExtractor={(item) => item.id}
            />
        );
    }

    filterItems(items, filters) {
        let result = [];
        filters.forEach(filter => {
            let filtered = items.filter((item)=>{
                return item[filter.name] == filter.value
            });
            result = [...result, ...filtered];
        });
        result.sort((a,b)=>{
            return a.id - b.id;
        })
        return result;
    }
}
