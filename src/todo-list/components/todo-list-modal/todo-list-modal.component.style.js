import React from 'react';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        ...StyleSheet.absoluteFillObject,
    },
    modalContent: {
        backgroundColor: "#ffffff",
        padding: 20,
        justifyContent: "center",
        // alignItems: "center",
        borderRadius: 2,
        borderColor: "rgba(0, 0, 0, 0.1)"
    },
    button:{
        height: 50,
        marginTop:10
    }
});

export default styles;