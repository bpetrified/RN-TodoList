import React, { Component, PropTypes } from 'react';
import { View, ScrollView } from 'react-native';
//styles
import styles from './todo-list-modal.component.style';
import buttonStyles from '../../../shared/styles/button';
import typoStyles from '../../../shared/styles/typo';
//Components
import Modal from 'react-native-modal';
import CustomText from '../../../shared/components/custom-text/custom-text.component';
import TodoListForm from '../todo-list-form/todo-list-form.component';
import Button from '../../../shared/components/button/button.component';
import LoadingOverlay from '../../../shared/components/loading-overlay/loading-overlay.component';

export default class ContactModal extends Component {
    render() {
        const { item, onCancel, onSave, loading } = this.props;

        return (
            <View style={styles.container} >
                <Modal isVisible={true} backdropOpacity={0.5} avoidKeyboard={true}>
                    <View style={styles.modalContent}>
                        <ScrollView>
                            <TodoListForm onSave={onSave} />
                            <Button style={[buttonStyles.red, styles.button]}
                                //disabled={invalid || loginFailed}
                                onPress={() => { }}>
                                <CustomText style={typoStyles.white}>Remove</CustomText>
                            </Button>
                            <Button style={[buttonStyles.outlineBlue, styles.button]}
                                //disabled={invalid || loginFailed}
                                onPress={() => { onCancel(); }}>
                                <CustomText style={typoStyles.primaryBlue}>Cancel</CustomText>
                            </Button>
                        </ScrollView>
                        {loading && <LoadingOverlay />}
                    </View>
                </Modal>
            </View>
        );
    }


}