import React from 'react';
import { StyleSheet } from 'react-native';
import appConstants from '../constant';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appConstants.color.lightGray,
    padding: 20,
    paddingTop: 40
  },
  content: {
    flex: 1,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 20
  },
  header: {
    flex: 0.1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  body: {
    flex: 0.9,
  },
  newButton: {
    width: 30,
    height: 30
  }
});

export default styles;