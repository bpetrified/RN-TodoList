import todoListConstants from './todo-list.constant';

export default todoListActions = {
    showModal(item){
        return {
            type: todoListConstants.showModal,
            data: item
        };
    },

    closeModal(){
        return {
            type: todoListConstants.closeModal
        };
    },

    getItems(){
        return {
            type: todoListConstants.getItems
        };
    },

    getItemsFinish(items){
        return {
            type: todoListConstants.getItemsFinish,
            data: items
        }
    },

    save(item){
        return {
            type: todoListConstants.save,
            data: item
        };
    },

    saveFinish(){
        return {
            type: todoListConstants.saveFinish
        };
    },

    remove(id){
        return {
            type: todoListConstants.remove,
            data: id
        };
    },

    removeFinish(){
        return {
            type: todoListConstants.removeFinish
        };
    },

    toggleFilter(filter){
        return {
            type: todoListConstants.toggleFilter,
            data: filter
        }
    }
}