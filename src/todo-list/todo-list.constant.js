export default todoListConstants = {
    //actions
    showModal: 'SHOW_MODAL',
    closeModal: 'CLOSE_MODAL',
    save: 'SAVE',
    saveFinish: 'SAVE_FINISH',
    remove: 'REMOVE',
    removeFinish: 'REMOVE_FINISH',
    getItems: 'GET_ITEMS',
    getItemsFinish: 'GET_ITEMS_FINISH',
    toggleFilter: 'TOGGLE_FILTER',
    //others
    filterList: [
        { id: 1, name: 'completed', value: true, label: 'Completed' },
        { id: 2, name: 'completed', value: false, label: 'In Completed' }
    ]
}