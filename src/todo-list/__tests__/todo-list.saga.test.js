import { call, put, take } from 'redux-saga/effects';
import sagaHelper from 'redux-saga-testing';
import { doSave, doGetItems } from '../todo-list.saga';
import todoListActions from '../todo-list.action';
//services
import itemService from '../../shared/services/item.service';

//just to show you how we can test saga
describe('Todo List saga', () => {
    describe('doSave func', () => {
        const mockAction = {
            data: 'this is item'
        };

        const it = sagaHelper(doSave(mockAction));

        it('should call service to save item', (result) => {
            expect(result).toEqual(call(itemService.save, mockAction.data));
        });

        it('then should getItems', (result) => {
            expect(result).toEqual(call(doGetItems));
        });

        it('then should put finish', (result) => {
            expect(result).toEqual(put(todoListActions.saveFinish()));
        });

        it('and finallt should put modalClose', (result) => {
            expect(result).toEqual(put(todoListActions.closeModal()));
        });
    });
});
