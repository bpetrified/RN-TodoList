import React, { Component } from 'react';
import { Image, View } from 'react-native';
//styles
import styles from './todo-list.component.style';
import typoStyles from '../shared/styles/typo';
import buttonStyles from '../shared/styles/button';
//components
import CustomText from '../shared/components/custom-text/custom-text.component';
import Button from '../shared/components/button/button.component';
import TodoListFlatList from './components/todo-list-flatlist/todo-list-flatlist.component';
import TodoListModal from './components/todo-list-modal/todo-list-modal.component';
import TodoListFilter from './components/todo-list-filter/todo-list-filter.component';
import LoadingOverlay from '../shared/components/loading-overlay/loading-overlay.component';

export default class TodoList extends Component {
    render() {
        const { state, actions } = this.props;
        
        return (<View style={styles.container}>
            <View style={styles.content}>
                <View style={styles.header}>
                    <CustomText style={typoStyles.large}>
                        Reminders
                    </CustomText>
                    <Button style={[buttonStyles.primaryBlue, styles.newButton]}
                        onPress={actions.showModal.bind(this)}
                    >
                        <CustomText style={typoStyles.white}>+</CustomText>
                    </Button>
                </View>
                <TodoListFilter 
                    onFilterChanged={actions.toggleFilter.bind(this)}
                    selectedFilters={state.selectedFilters}/>
                <View style={[styles.body]}>
                    <TodoListFlatList 
                        datas={state.items}
                        selectedFilters={state.selectedFilters}
                        onItemPress={actions.showModal.bind(this)}
                        onCheckboxChanged={actions.save.bind(this)}
                    />
                </View>
            </View>
            { state.modalShown && <TodoListModal item={state.selectedItem} 
                onCancel={actions.closeModal.bind(this)}
                onSave={actions.save.bind(this)}
                loading={state.loading}
            />}
            {state.loading && <LoadingOverlay/> }
        </View>)
    }


}
