export default appConstants = {
    color: {
        primaryBlue: '#007ab3',
        error: '#dc3149',
        softGray: '#eeeeee',
        gray: '#909090',
        darkGray: '#414141',
        lightGray: '#E2E2E2'
    }
};