import todoListSaga from './todo-list/todo-list.saga';
import {all} from 'redux-saga/effects';

export default function* rootSaga() {
    yield all([
        ...todoListSaga
    ]);
}