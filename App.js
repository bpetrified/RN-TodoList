//@flow
import React from 'react';
import { Provider } from 'react-redux';
import store from './src/store';
import TodoListScreen from './src/todo-list/todo-list.screen';
import { View } from 'react-native';

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <TodoListScreen/>
      </Provider>
    );
  }
}